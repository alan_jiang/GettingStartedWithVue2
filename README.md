## Vue快速入门
```
.
├── xxx.html                                 Vue实例
├── README.md                                当前文件
├── css                                      实例css文件
├── doc                                      演示文稿
└── img                                      实例用到图片
```

## 怎么使用
1, 安装Node.js
> https://nodejs.org/en/ 下载LTS版本

2, 安装live-server
> npm install live-server -g

3, 运行
> live-server --port=8899

4, 在浏览器上访问
> http://localhost:8899

## 插件
在doc/Vue.js_devtools v5.3.4.zip为Vue开发常用Chrome插件.
